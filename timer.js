function Initialize(){

	_ =  {};
	var Promise = require("promise");
	var ref;
	_.wait = function(ms) {
		var p = new Promise(function(resolve, reject) {
			ref = setInterval(function() {
				clearInterval(ref);
				resolve();
			}, ms);
		});
		return p;
	}

	return _;
}

module.exports = Initialize;