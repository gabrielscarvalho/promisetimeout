

function Initialize() {
	var resolve;
	var reject;

	_ = {};
	_.hasComplete = false;

	_.loadPromise = function (resolveFn, rejectFn) {
		resolve = resolveFn;
		reject = rejectFn;
		this.hasComplete = false;
	}

	_.resolve = function (args) {
		if (!this.hasComplete) {
			this.hasComplete = true;
			resolve.apply(this, arguments);
		} 
	}

	_.reject = function (args) {
		if (!this.hasComplete) {
			this.hasComplete = true;
			reject.apply(this, arguments);
		} 
	}
	return _;
}

module.exports = Initialize;