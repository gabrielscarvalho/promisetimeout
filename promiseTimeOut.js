function Initialize() {
	var Promise = require("promise");
	
	_ = function (func, timeOut, message) {
	
		var holder = require("./holder")();
		var timer = require("./timer")();

		var p = new Promise(function (resolve, reject) {
			holder.loadPromise(resolve, reject);
			func(holder);

			if (timeOut != null) {
				timer.wait(timeOut).then(function () {
					holder.reject({ timeOut: true, message: message });
				})
			}
		});
		return p;
	}
	return _;
}
module.exports = Initialize();