var PromiseTimeOut = require("./promiseTimeOut");
var timer = require("./timer")();



var promiseA = new PromiseTimeOut(function (holder) {

	console.log("[A] Searching...");
	
	timer.wait(4000).then(function () {
		holder.resolve({ 'info': ' info "A"' });
	});

}, 3000, "[A] time > 3 seconds.").then(function (success) {
	console.log("[A] Success: " + success.info);
}, function (err) {
	console.log("[A] Error: " + err.message);
});

//Need to instanciate again to work.
timer = require("./timer")();


var promiseB = new PromiseTimeOut(function (holder) {
	console.log("[B] Searching...");
	
	timer.wait(1000).then(function () {
		holder.resolve({ 'info': 'info "B"' });
	});

}, 2000, "[B] time > 2 seconds.").then(function (success) {
	console.log("[B] Success: " + success.info);
}, function (err) {
	console.log("[B] Error: " + err.message);
});
